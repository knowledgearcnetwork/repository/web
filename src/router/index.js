import Vue from 'vue'
import Router from 'vue-router'
import Repository from '@/components/Repository'
import Item from '@/components/Item'
import Settings from '@/components/Settings'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Repository',
            component: Repository,
        },
        {
            path: '/add',
            name: 'Item',
            component: Item
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Settings
        }
    ]
})

var Archive = artifacts.require("./Archive.sol");

contract('Archive', function(accounts) {
    it("saves a hash", async function() {
        const owner = accounts[0]
        const hash = "QmQnBuMf4xKFvv3WuTCQVS5JnSnRz3qCijE3tT7fZtrWe8"

        const archive = await Archive.deployed()

        await archive.save(hash, {from: owner})

        assert.equal(await archive.get({from: owner}), hash)
    })
});
